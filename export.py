import sys,os,shutil

def makedir(d):
    if not os.path.exists(d):
        os.makedirs(d)

src = os.path.join(sys.path[0],"PuzzleSRC")
dest = "C:\Users\Matt\Desktop\PuzzleSRC"

files = ["Run","PuzzleUtils","SearchController","SearchType","SearchUtils"]

makedir(dest)
makedir(os.path.join(dest,"maps"))

for each in files:
    fname = each+".py"
    fname = os.path.join(src,fname)
    shutil.copyfile(fname, dest)

