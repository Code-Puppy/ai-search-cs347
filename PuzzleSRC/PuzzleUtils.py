from SearchUtils import *
from SearchType import *
import SearchController

#####################################################
# PuzzleUtils.py by Matthew Terneus <mrtf38@mst.edu>
# Updated 1/28/12
# Contains classes: MapArray, MapState
# Which describe the state of person traveling a
# topilogical grid
# Dependancies: SearchUtils, SearchType, SearchController
#####################################################

#### MapArray class from PuzzleUtils.py
# Updated 1/18/12
# Describes a topilogical square grid
class MapArray:
    ## Constructor for MapArray
    # Creates grid of side length size with heights contained in array
    # Every size elements represents one row of grid
    # Args: int size, list array 
    def __init__(self,size,array):
        self.size = size
        self.array = array
        # Find smallest step cost
        self.unitcost = self.delta(0,1)
        for i in range(len(array)):
            if 0 <= i//size - 1:
                self.unitcost = min(self.unitcost,self.delta(i,i-size))
            if size > i//size + 1:
                self.unitcost = min(self.unitcost,self.delta(i,i+size))
            if 0 <= i%size - 1:
                self.unitcost = min(self.unitcost,self.delta(i,i-1))
            if size > i%size + 1:
                self.unitcost = min(self.unitcost,self.delta(i,i+1))

                
    ## delta from MapArray
    # Finds height differance between points on grid
    # Args: int pos1, int pos2
    # Returns: float height differance
    def delta(self,pos1,pos2):
        return abs(self.array[pos1]-self.array[pos2])

######################################################################

#### MapState class from PuzzleUtils.py
# Updated 2/24/12
# Describes a topilogical square grid
# Inherits: State in SearchTreeUtils
class MapState (State):
    ## Constructor for MapState
    # Creates state object that holds map data and position on map
    # Args: MapArray maparray, int pos 
    def __init__(self, maparray, position):
        self.maparray = maparray
        self.pos = position
        
    ## move from MapState
    # Attempts to create a new state object
    # representing the state after an action
    # Args: str action
    # Returns: New state/ current state if failed
    def move(self,action):
        if action == "North" and self.pos >= self.maparray.size:
            return MapState(self.maparray,self.pos-self.maparray.size)
        elif action == "South" and self.pos < self.maparray.size**2-self.maparray.size:
            return MapState(self.maparray,self.pos+self.maparray.size)
        elif action == "West" and self.pos%self.maparray.size > 0 :
            return MapState(self.maparray,self.pos-1)
        elif action == "East" and self.pos%self.maparray.size < self.maparray.size-1 :
            return MapState(self.maparray,self.pos+1)
        else:
            print "Warning: Invalid Move Attempted"
            return self

    ## move from MapState
    # Attempts to create a new state object
    # representing the state prior to the action
    # Args: str action
    # Returns: New state/ current state if failed
    def moveBackward(self,action):
        if action == "South" and self.pos >= self.maparray.size:
            return MapState(self.maparray,self.pos-self.maparray.size)
        elif action == "North" and self.pos < self.maparray.size**2-self.maparray.size:
            return MapState(self.maparray,self.pos+self.maparray.size)
        elif action == "East" and self.pos%self.maparray.size > 0 :
            return MapState(self.maparray,self.pos-1)
        elif action == "West" and self.pos%self.maparray.size < self.maparray.size-1 :
            return MapState(self.maparray,self.pos+1)
        else:
            print "Warning: Invalid Move Attempted"
            return self

    ## getValidMoves from MapState
    # Creates a list of all possible moves from this state
    # Returns: tuple(list [str], cost)
    def getValidMoves(self):
        actionlist = []
        if self.pos < self.maparray.size**2-self.maparray.size:
            d = self.maparray.delta(self.pos,self.pos+self.maparray.size)
            actionlist.append(("South",d))
        if self.pos%self.maparray.size < self.maparray.size-1:
            d = self.maparray.delta(self.pos,self.pos+1)
            actionlist.append(("East",d))
        if self.pos >= self.maparray.size:
            d = self.maparray.delta(self.pos,self.pos-self.maparray.size)
            actionlist.append(("North",d))
        if self.pos%self.maparray.size > 0:
            d = self.maparray.delta(self.pos,self.pos-1)
            actionlist.append(("West",d))
        return actionlist

    ## getReverseMoves from MapState
    # Creates a list of all possible moves to this state
    # Returns: tuple(list [str], cost)
    def getReverseMoves(self):
        actionlist = []
        if self.pos >= self.maparray.size:
            d = self.maparray.delta(self.pos,self.pos-self.maparray.size)
            actionlist.append(("South",d))
        if self.pos%self.maparray.size > 0:
            d = self.maparray.delta(self.pos,self.pos-1)
            actionlist.append(("East",d))
        if self.pos < self.maparray.size**2-self.maparray.size:
            d = self.maparray.delta(self.pos,self.pos+self.maparray.size)
            actionlist.append(("North",d))
        if self.pos%self.maparray.size < self.maparray.size-1:
            d = self.maparray.delta(self.pos,self.pos+1)
            actionlist.append(("West",d))
        return actionlist

    ## equals from MapState
    # Compares two MapState objects
    # Args: MapState state
    # Returns: True if states are the same
    def equals(self,state):
        return self.pos == state.pos

    ## toStr from MapState
    # Creates co-ords of state for displaying
    # Returns: str of co-ords
    def toStr(self):
        return "("+str(self.pos%self.maparray.size)+","+str(self.pos//self.maparray.size)+")"

###################################################



# The Following are Hueristic Functions for the Puzzle problem



#### compositeEst funtion in PuzzleUtils.py
# Estimates cost to get to goal using a compsite heuristic
# Returns: int cost
def compositeEst(state):
    return max(netDeltaEst(state),unitStepEst(state))

#### netDeltaEst funtion in PuzzleUtils.py
# Estimates cost to get to goal as net distance left to travel
# Good estimate if only moving down or up hill
# Returns: int cost
def netDeltaEst(state):
    return state.maparray.delta(state.pos,state.maparray.size**2-1)

#### unitStepEst funtion in PuzzleUtils.py
# Estimates cost to get to goal as minimum steps to goal times unit(smallest) cost
# Decent estimate if at near same height but there is a valley or mountain in the way
# Returns: int cost
def unitStepEst(state):
    return (2*state.maparray.size -2 - state.pos//state.maparray.size - state.pos%state.maparray.size) * state.maparray.unitcost

#### minEdgeEst funtion in PuzzleUtils.py
# Estimates cost to get to goal as smallest next possible step
# Best First like weighting, NOTE: Dominated by by netDeltaEst
# Returns: int cost
def minEdgeEst(state):
    mincost = float("inf")
    size = state.maparray.size
    if 0 <= state.pos//state.maparray.size - 1:
        mincost = min(mincost,state.maparray.delta(state.pos,state.pos-state.maparray.size))
    if state.maparray.size > state.pos//state.maparray.size + 1:
        mincost = min(mincost,state.maparray.delta(state.pos,state.pos+state.maparray.size))
    if 0 <= state.pos%state.maparray.size - 1:
        mincost = min(mincost,state.maparray.delta(state.pos,state.pos-1))
    if state.maparray.size > state.pos%state.maparray.size + 1:
        mincost = min(mincost,state.maparray.delta(state.pos,state.pos+1))
    return mincost

        
