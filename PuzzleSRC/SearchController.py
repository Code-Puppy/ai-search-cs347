from SearchUtils import *

#####################################################
# SearchController.py by Matthew Terneus <mrtf38@mst.edu>
# Updated 2/8/12
# Contains classes: SearchController, StandardSearch,
# IterativeSearch, BiDirectionalSearch, IterativeBiDirectionalSearch
# Used to Control the search loop through the frontier of search tree
# Dependancies: SearchUtils
#####################################################

#### SearchController ABSTRACT class from SearchController.py
# Updated 2/4/12
# Contains methods search the frontier and return a usable result
class SearchController:
    ## run from SearchController
    # Links the search and parse
    # Args: Node start, list[Node] goal, Frontier searchtype, Anything args
    # Returns: tuple(list[tuple(action,state)],cost)
    def run(self,start,goal,searchtype,args = None):
        return self.parseBranch(self.search(start,goal,searchtype,args))
    
    ## search ABSTRACT from SearchController
    # main loop for searching the frontier
    # Args: Node start, list[Node] goal, Frontier searchtype, Anything args
    # Returns: Node containing goal
    def search(self,start,goal,searchtype,args = None):
        return None

    ## parseBranch from SearchController
    # Backtracks up tree to find the solution
    # Args: Node endnode
    # Returns: tuple(list[tuple(action,state)],cost)
    def parseBranch(self,endnode):
        if endnode == None:
            return (("Failure",""),0)
        crntnode = endnode
        actionset = []
        #while the root of the tree has not been passed
        while crntnode != None:
            #put action and state on the front of the list
            actionset.insert(0,(crntnode.action,crntnode.state.toStr()))
            crntnode = crntnode.parent
        return (actionset,endnode.cost)

#####################################################################################

#### StandardSearch class from SearchController.py
# Updated 2/4/12
# Contains methods search the frontier and return a usable result
# Inherits: SearchController
class StandardSearch(SearchController):
    ## search  from StandardSearch
    # main loop for searching the frontier
    # Args: Node start, list[Node] goal, Frontier searchtype, unused args
    # Returns: Node containing goal
    def search(self,start,goal,searchtype,args = None):
        #create root node
        root = Node(start,None,"Start",0)
        #initialize frontier as type searchtype
        frontier = searchtype(root,None,args)
        #Create Counter for nodes explored to compare effectiveness of huerisitics
        self.nodecount = 0
        #while nodes can be explored
        while(not frontier.empty()):
            # Pick Node
            node = frontier.pop()
            self.nodecount = self.nodecount+1
            # Check if goal
            for each in goal:
                if each.equals(node.state):
                    # Return end of branch for parsing
                    return node
            # expand new nodes into the frontier
            node.expand(frontier)
        #if no more nodes to explore search failed
        return None

#####################################################################################

#### IterativeSearch class from SearchController.py
# Updated 2/4/12
# Contains methods search the frontier iteratively and return a usable result
# Inherits: SearchController
class IterativeSearch(SearchController):
    ## search  from IterativeSearch
    # main loop for searching the frontier
    # Args: Node start, list[Node] goal, Frontier searchtype, depthlimit args
    # Returns: Node containing goal
    def search(self,start,goal,searchtype,args = None):
        #create root node
        root = Node(start,None,"Start",0)
        depth = 0
        #Create Counter for nodes explored to compare effectiveness of huerisitics
        self.nodecount = 0
        while True:
            #initialize frontier as type searchtype
            frontier = searchtype(root,depth,args)
            #while nodes can be explored
            while(not frontier.empty()):
                # Pick Node
                node = frontier.pop()
                self.nodecount = self.nodecount+1
                # Check if goal
                for each in goal:
                    if each.equals(node.state):
                        # Return end of branch for parsing
                        return node
                # expand new nodes into the frontier
                node.expand(frontier)
            #if no more nodes to explore search failed increase depth
            if hasattr(frontier,"depthstep"):
                # Use built in increment if availiable
                depth = frontier.depthstep
                if depth == -1:
                    break
            else:
                depth = depth + 1
        return None

#####################################################################################

#### BiDirectionalSearch class from SearchController.py
# Updated 2/4/12
# Contains methods search the frontier in two directions and return a usable result
# Inherits: SearchController
class BiDirectionalSearch(SearchController):
    ## search  from BiDirectionalSearch
    # main loop for searching the frontier
    # Args: Node start, list[Node] goal, Frontier searchtype, unused args
    # Returns: Node containing goal
    def search(self,start,goal,searchtype,args = None):
        # create root node
        root = Node(start,None,"Start",0)
        # initialize frontiers as type searchtype
        front = searchtype(root,None,args)
        back = searchtype(None,None,args)
        # fill back frontier with all goals
        for each in goal:
            newnode = Node(each,None,"End",0)
            back.push(newnode)

        # while nodes can be explored
        while (not front.empty()) and (not back.empty()):
            # Pick Node
            forward = front.pop()
            backward = back.pop()
            # check if the picked node is linked to the other frontier
            for each in back.toList():
                if each.state.equals(forward.state):
                    return self.splice(forward, each)
            for each in front.toList():
                if each.state.equals(backward.state):
                    return self.splice(each, backward)
            forward.expand(front)
            backward.expandBackward(back)
        #if no more nodes to explore search failed
        return None
    
    ## splice  from BiDirectionalSearch
    # combines a forward and backward brach into one forward branch
    # Args: Node start, list[Node] goal, Frontier searchtype, unused args
    # Returns: Node containing goal
    def splice(self,frontbranch, backbranch):
        nextnode = backbranch.parent
        lastnode = frontbranch
        lastaction = backbranch.action
        # while there is a parent on the backward branch
        while nextnode!= None:
            #shuffle parents and actions
            currentnode = nextnode
            nextaction = currentnode.action
            currentnode.action = lastaction
            lastaction = nextaction
            nextnode = currentnode.parent
            currentnode.parent = lastnode
            lastnode = currentnode
        #store the net path cost on the last node so that it can be returned with the solution
        lastnode.cost = frontbranch.cost + backbranch.cost
        return lastnode

#####################################################################################

#### IterativeBiDirectionalSearch class from SearchController.py
# Updated 2/4/12
# Contains methods search the frontier in two directions iteratively and return a usable result
# Inherits: BiDirectionalSearch
class IterativeBiDirectionalSearch(BiDirectionalSearch):
    ## search  from IterativeBiDirectionalSearch
    # main loop for searching the frontier
    # Args: Node start, list[Node] goal, Frontier searchtype, depthlimit args
    # Returns: Node containing goal
    def search(self,start,goal,searchtype,args = None):
        depth = 0
        while True:
            #create root node
            root = Node(start,None,"Start",0)
            # initialize frontiers as type searchtype
            front = searchtype(root,depth,args)
            back = searchtype(None,depth,args)
            # fill back frontier with all goals
            for each in goal:
                newnode = Node(each,None,"End",0)
                back.push(newnode)

            while (not front.empty()) and (not back.empty()):
                # Pick Nodes
                forward = front.pop()
                backward = back.pop()
                # check if the picked node is linked to the other frontier
                for each in back.toList():
                    if each.state.equals(forward.state):
                        return self.splice(forward, each)
                for each in front.toList():
                    if each.state.equals(backward.state):
                        return self.splice(each, backward)
                forward.expand(front)
                backward.expandBackward(back)
            #if no more nodes to explore search failed try deeper
            depth = depth+1
        return None
