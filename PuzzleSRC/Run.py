from PuzzleUtils import *
from SearchType import *
import SearchController
import sys,os

#####################################################
# Run.py by Matthew Terneus <mrtf38@mst.edu>
# Updated 2/4/12
# Executing Script
# Reads file, generates initial and goal states, uses UCGS
# Dependancies: SearchController, SearchType, PuzzleUtils
# PyModules: sys, os
#####################################################

directory = sys.path[0] #default file search location
contin = True # Flag tracks if reading of file is working correctly

#Get file name
inpt = sys.argv[1]
fname = os.path.join(directory,"maps",inpt)
if not os.path.isfile(fname):
    #try alternate location
    if os.path.isfile(inpt):
        fname = inpt
    else:
        print "Failure: Could not locate file"
        print "Please use full path name or place file in ThisDirectory/maps/"
        contin = False

if contin:
    #store lines of file in array
    filedata= file(fname)
    mapdata = filedata.readlines()
    filedata.close()

    #read map size
    mapsize = int(mapdata[0])
    valuearray = []

    #checks number of lines
    if mapsize != len(mapdata)-1:
        print "Failure: The file is not structured properly(file length)"
        contin = False

if contin:
    for line in range(mapsize):
        #get individual values
        values = mapdata[1+line].rstrip(" /n").split(" ")
        #check if enough values in the line
        if mapsize != len(values)-1:
            print "Failure: The file is not structured properly(line length)"
            contin = False
            break
        for each in range(len(values)-1):
            #add values to the list
            valuearray.append(float(values[each]))
if contin:
    #generate states
    maparray = MapArray(mapsize,valuearray)
    initstate = MapState(maparray,0)
    goalstates = [MapState(maparray,mapsize**2-1)]
    
    #Creating Search agents
    AI = SearchController.StandardSearch()
    AI2 = SearchController.IterativeSearch()

    #Searching with first agent
    print "Calculating with ASTAR:"
    print ""
    print "Heuristic: lowest possible step cost as unit step"
    (solution,cost) = AI.run(initstate,goalstates,ASTAR,unitStepEst)
    print "Solution:"
    print solution
    print "Path Cost:"
    print cost
    print "Nodes Explored:"
    print AI.nodecount
    print ""
    print "Heuristic: Composite of unit step and net height change from state to goal"
    (solution,cost) = AI.run(initstate,goalstates,ASTAR,compositeEst)
    print "Solution:"
    print solution
    print "Path Cost:"
    print cost
    print "Nodes Explored:"
    print AI.nodecount
    print ""
    print ""
    print "Calculating with ID-ASTAR:"
    print ""
    print "Heuristic: lowest possible step cost as unit step"
    (solution,cost) = AI2.run(initstate,goalstates,ASTAR,unitStepEst)
    print "Solution:"
    print solution
    print "Path Cost:"
    print cost
    print "Nodes Explored:"
    print AI2.nodecount
    print ""
    print "Heuristic: Composite of unit step and net height change from state to goal"
    (solution,cost) = AI2.run(initstate,goalstates,ASTAR,compositeEst)
    print "Solution:"
    print solution
    print "Path Cost:"
    print cost
    print "Nodes Explored:"
    print AI2.nodecount
    print ""
