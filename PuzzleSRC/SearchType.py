from SearchUtils import Frontier
from collections import deque
import Queue, heapq

#####################################################
# SearchType.py by Matthew Terneus <mrtf38@mst.edu>
# Updated 1/28/12
# Contains classes: BFTS, UCGS, DLTS
# Used to navigate the frontier of a tree search
# Dependancies: SearchUtils
# Py Modules: Queue, heapq
#####################################################

#### BFTS class from PuzzleUtils.py
# Updated 1/28/12
# Contains methods to preform a Breadth First Tree Search
# Inherits: Frontier in SearchTreeUtils
class BFTS(Frontier):
    ## new from BFTS
    # Extends the default init of the frontier
    # Creates queue
    # Args: unused args
    def new(self,args):
        self.queue = deque()

    ## push from BFTS
    # stores a node on the frontier in a FIFO queue
    # Args: Node item
    def push(self,item):
        self.queue.append(item)

    ## pop from BFTS
    # Picks a node off the queue
    # Returns: Node 
    def pop(self):
        return self.queue.popleft()        

    ## pop from BFTS
    # Checks is queue is empty
    # Returns: True if empty
    def empty(self):
        return len(self.queue) == 0

    ## toList from BFTS
    # returns queue as a list
    # Returns list
    def toList(self):
        return self.queue
    
###########################################################################
    
#### BFGS class from PuzzleUtils.py
# Updated 1/28/12
# Contains methods to preform a Breadth First Graph Search
# Inherits: Frontier in SearchTreeUtils
class BFGS(BFTS):
    ## new from BFGS
    # Extends the default init of the frontier
    # Creates queue
    # Args: unused args
    def new(self,args):
        self.queue = deque()
        self.closed = []

    ## push from BFGS
    # stores a node on the frontier in a FIFO queue
    # Args: Node item
    def push(self,item):
        mode = 0 #Open mode

        for each in self.closed:
            if each.equals(item.state):
                mode = 1 # Closed mode

        if mode == 0:
            self.queue.append(item)
            self.closed.append(item.state)
    
###########################################################################

#### UCGS class from SearchType.py
# Updated 1/22/12
# Contains methods to preform a Uniform Cost Graph Search
# Inherits: Frontier in SearchTreeUtils
class UCGS(Frontier):
    ## new from UCGS
    # Extends the default init of the frontier
    # Creates queue
    # Args: unused args
    def new(self,args):
        self.heap = []
        self.found = []
        self.closed = []

    ## push from UCGS
    # stores a node on the frontier in a min heap
    # if it hasn't already been visted, or updates node
    # Args: Node item
    def push(self,item):
        mode = 0
        for each in self.found:
            if each.equals(item.state):
                mode = 1
                break
        for each in self.closed:
            if each.equals(item.state):
                mode = 2
                break
            
        if mode == 0: #Make node
            heapq.heappush(self.heap,(item.cost,item))
            self.found.append(item.state)
        elif mode == 1:#Update node
            for index in range(len(self.heap)):
                (cost,node) = self.heap[index]
                if node.state.equals(item.state) and cost > item.cost:
                    self.heap[index] = (item.cost,item)
                    heapq.heapify(self.heap)
                    break
                
    ## pop from UCGS
    # Picks a node off the top if the heap
    # Returns: Node 
    def pop(self):
        #get node
        (cost,node) = heapq.heappop(self.heap)
        #remove state from found list
        for index in range(len(self.found)):
            if self.found[index].equals(node.state):
                self.found[index:index+1] = []
                break
        #add state to closed list
        self.closed.append(node.state)
        return node

    ## pop from UCGS
    # Checks is heap is empty
    # Returns: True if empty
    def empty(self):
        return len(self.heap) == 0

    ## toList from UCGS
    # returns heap as a list
    # Returns list
    def toList(self):
        return self.heap

#####################################################################################

#### DLTS class from SearchType.py
# Updated 1/28/12
# Contains methods to preform a Depth (First) Limited Tree Search
# Inherits: Frontier in SearchTreeUtils
class DLTS(Frontier):
    ## new from DLTS
    # Extends the default init of the frontier
    # Creates stack
    # Args: int args
    def new(self,args):
        self.stack = deque()

    ## push from DLTS
    # stores a node on the stack if it is in the depth limit
    # Args: Node item
    def push(self,item):
        if depth == None or item.depth <= self.depth:
            self.stack.append(item)

    ## pop from DLTS
    # Picks a node off the top of the stack
    # Returns: Node 
    def pop(self):
        return self.stack.pop()

    ## pop from DLTS
    # Checks is heap is empty
    # Returns: True if empty
    def empty(self):
        return len(self.stack) == 0

    ## toList from DLTS
    # returns stack as a list
    # Returns list
    def toList(self):
        return self.stack

###########################################################################

#### ASTAR class from SearchType.py
# Updated 2/4/12
# Contains methods to preform a A* graph search
# Inherits: Frontier in SearchTreeUtils
class ASTAR(Frontier):
    ## new from ASTAR
    # Extends the default init of the frontier
    # Creates queue
    # Args: heuristic function args or list[function,cost limit]
    def new(self,args):
        self.heap = []
        self.found = []
        self.closed = []
        self.heuristic = args
        self.depthstep = -1

    ## push from ASTAR
    # stores a node on the frontier in a min heap
    # if it hasn't already been visted, or updates node
    # Args: Node item
    def push(self,item):
        mode = 0
        index = None
        # Estimate pathcost
        estcost = item.cost+ self.heuristic(item.state)

        # Check if in frontier
        for each in range(len(self.found)):
            if self.found[each].equals(item.state):
                mode = 1 # Must Update
                index = each
                break

        # Check if closed
        for each in self.closed:
            if each.equals(item.state):
                mode = 2 # Can't Update
                break
            
        # Check cost limit
        if self.depth != None and estcost > self.depth:
            mode = 2 # Can't Update
            # Record smallest cost of disregarded nodes
            if estcost < self.depthstep  or self.depthstep  == -1:
                self.depthstep  = estcost 
                
        if mode == 0: # Make node
            heapq.heappush(self.heap,(item.cost,item))
            self.found.append(item.state)
        elif mode == 1:# Update node
            (cost,node) = self.heap[index]
            if node.state.equals(item.state) and cost > estcost:
                # Replace old node
                self.heap[index] = (item.cost,item)
                # Re-establish heap
                heapq.heapify(self.heap)
                
    ## pop from ASTAR
    # Picks a node off the top if the heap
    # Returns: Node 
    def pop(self):
        #get node
        (cost,node) = heapq.heappop(self.heap)
        #remove state from found list
        for index in range(len(self.found)):
            if self.found[index].equals(node.state):
                self.found[index:index+1] = []
                break
        #add state to closed list
        self.closed.append(node.state)
        return node

    ## pop from ASTAR
    # Checks is heap is empty
    # Returns: True if empty
    def empty(self):
        return len(self.heap) == 0

    ## toList from ASTAR
    # returns heap as a list
    # Returns list
    def toList(self):
        return self.heap
