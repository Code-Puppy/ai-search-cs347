#####################################################
# SearchUtils.py by Matthew Terneus <mrtf38@mst.edu>
# Updated 2/4/12
# Contains classes: Node, Frontier, State
# Tools used to preform a AI search
#####################################################

#### Node class from SearchUtils.py
# Updated 2/4/12
# Describes a node in the search tree
# Which records its state, parent, action to get there, and pathcost
class Node:
    ## Constructor for Node
    # Records descriptors
    # Args: State state, Node parent, str action, float cost 
    def __init__(self,state,parent,action,cost,depth = 0):
        self.state = state
        self.parent = parent
        self.action = action
        self.cost = cost
        self.depth = depth

    ## expand from Node
    # Creates new nodes based on all possible actions
    # from state and adds them to frontier
    # Args: Frontier frontier    
    def expand(self,frontier):
        #get all action,cost tupples from all valid moves in state
        for(newaction,newcost) in self.state.getValidMoves():
            #create new node with new state recording parent and action and tallying cost
            newnode = Node(self.state.move(newaction),self,newaction,self.cost+newcost,self.depth+1)
            #Put the new node on the frontier
            frontier.push(newnode)

    ## expandBackward from Node
    # Creates new nodes based on all possible reversed actions
    # from state and adds them to frontier
    # Args: Frontier frontier    
    def expandBackward(self,frontier):
        #get all action,cost tupples from all valid moves in state
        for(newaction,newcost) in self.state.getReverseMoves():
            #create new node with new state recording parent and action and tallying cost
            newnode = Node(self.state.moveBackward(newaction),self,newaction,self.cost+newcost,self.depth+1)
            #Put the new node on the frontier
            frontier.push(newnode)
            

##########################################################################################

#### Frontier ABSTRACT class from SearchUtils.py
# Updated 2/8/12
# Keeps track and selects nodes on the leaves of search tree
class Frontier:
    ## Constructor for Frontier
    # Creates storage and adds root of tree to storage
    # Args: Node root, Anything args
    def __init__ (self,root,depth = None,args = None):
        self.depth = depth
        self.new(args)
        if root != None:
            self.push(root)
        
    ## new ABSTRACT from Frontier
    # Preforms firsttime setup for frontier
    def new(self, args):
        pass

    ## empty ABSTRACT from Frontier
    # Check if nodes left in frontier
    # Returns: True if frontier empty
    def empty(self):
        return True

    ## push ABSTRACT from Frontier
    # Adds node to frontier
    # Args: Node node
    def push(self,node):
        pass

    ## empty ABSTRACT from Frontier
    # Picks node from frontier
    # Returns: Node
    def pop(self):
        return None

    ## toList ABSTRACT from Frontier
    # returns frontier as a list
    # Returns list
    def toList(self):
        return []

#########################################################################################

#### State ABSTRACT class from SearchUtils.py
# Updated 2/4/12
# Stores data relating to a possible state
# and methods to determine states that can succeed this state
class State:
    ## move ABSTRACT from State
    # Create new State that would succeed if action was taken
    # Args: str action
    # Returns: State
    def move(self,action):
        return None

    ## getValidMoves ABSTRACT from State
    # Create list of actions that can be taken from this state
    # Returns: list [str]
    def getValidMoves(self):
        return []

    ## getReverseMoves ABSTRACT from State
    # Create list of actions that can be taken to get to this state
    # Returns: list [str]
    def getReverseMoves(self):
        return []
    
    ## equals ABSTRACT from State
    # Determine if state matches another
    # Returns: True if states are the same
    def equals(self,state):
        return True
    
    ## toStr ABSTRACT from State
    # Create string representation of state
    # Returns: str
    def toStr(self):
        return ""

